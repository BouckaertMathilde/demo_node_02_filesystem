# Setup projet
* Initialiser le projet node :
```
    npm init
```
* (Optionnel) Si initialisation git : penser au gitignore
* Créer le fichier app.js

# Pour importer la gestion de fichiers avec callbacks
https://nodejs.org/dist/latest-v10.x/docs/api/fs.html
```
    const fs = require('fs');
```

# Pour importer la gestion de fichiers avec les promesses
```
    const fs = require('fs/promises');
```
